#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <sstream>
#include <vector>

using namespace std;

template <typename Iter>
double median(Iter first, Iter last)
{
    return 0.0;
}

TEST_CASE("median")
{
    SECTION("odd number of numbers")
    {
        vector<int> data = {1, 3, 3, 6, 7, 8, 9};
        random_shuffle(begin(data), end(data));

        REQUIRE(median(begin(data), end(data)) == 6);
    }

    SECTION("even number of numbers")
    {
        vector<int> data = {1, 2, 3, 4, 5, 6, 8, 9};
        random_shuffle(begin(data), end(data));

        REQUIRE(median(begin(data), end(data)) == Approx(4.5));
    }
}