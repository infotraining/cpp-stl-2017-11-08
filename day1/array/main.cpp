#include <iostream>
#include <array>
#include <string>
#include <string.h>

// https://bitbucket.org/infotraining/cpp-stl-2017-11-08

using namespace std;

struct two_elements
{
    two_elements(std::initializer_list<int> l)
    {

    }
    int a;
    int b;
private:
    int c;
};

class test
{
public:
    test()
    {
        cout << "Ctor test" << endl;
    }
};

int main()
{
    two_elements te {1,2};

    cout << "Hello World!" << endl;
    std::array<int, 5> arr1; // equals int[5];
    std::array<int, 3> arr2 {1,2,3};
    std::array<int, 3> arr3{};
    std::array<test, 3> arr_test;

    for(size_t i = 0 ; i < arr1.size() ; ++i)
    {
        cout << arr1[i] << endl;
    }

    for (auto& el : arr2)
    {
        cout << el << endl;
    }

    for (auto& el : arr3)
    {
        cout << el << endl;
    }

    array<char, 100> buff{};
    strcpy(buff.data(), "Ala ma kota");

    return 0;
}
