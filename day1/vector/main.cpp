#include <iostream>
#include <vector>

using namespace std;

template<typename Cont>
void print(Cont c)
{
    for(const auto& el : c)
        cout << el << endl;
}

class Gadget
{
    int id_;
public:
    Gadget(int id) : id_(id) {
        cout << "Gadget ctor " << id_ << endl;
    }

    Gadget(const Gadget& rg) = delete;
    Gadget(Gadget&& rg)
    {
        id_ = rg.id_;
        cout << "Gadget mv ctor " << id_ << endl;
    }

    Gadget& operator=(const Gadget& rg) = delete;
    Gadget& operator=(Gadget&& rg)
    {
        id_ = rg.id_;
        cout << "Gadget cp ass" << id_ << endl;
        return *this;
    }

    ~Gadget()
    {
        cout << "Gadget dtor " << id_ << endl;
    }
};

int main()
{
    vector<int> v = {10, 20, 30};
    print(v);

    vector<int> v2(10);
    std::cout << v2.capacity() << endl;
    //print(v2);

    cout << "**************************" << endl;
    vector<int> v3(0);
    for (int i = 0 ; i < 10 ; ++i)
    {
        std::cout << "size = " <<  v3.size() << " cap = " << v3.capacity() << endl;
        v3.push_back(10);
    }
    cout << "Shrink to fit" << endl;
    v3.shrink_to_fit();
    std::cout << "size = " <<  v3.size() << " cap = " << v3.capacity() << endl;


    for(vector<int>::iterator it = v.begin() ; it != v.end() ; ++it)
    {
        *it *= 2;
    }

    for(vector<int>::const_iterator it = v.begin() ; it != v.end() ; ++it)
    {
        cout << *it << endl;
    }

    for(auto it = crbegin(v) ; it != crend(v) ; ++it)
    {
        cout << *it << endl;
    }

    v[1];
    v.at(1); // slower, throws

    v.insert(end(v), -100);
    print(v);

    // gadgets

    vector<Gadget> vg;
    vg.push_back(Gadget(1));

    vg.emplace_back(2);
    Gadget g3(3);
    vg.push_back(move(g3));

    return 0;
}
