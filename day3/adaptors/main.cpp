#include <iostream>
#include <queue>
#include <stack>

using namespace std;

int main()
{
    queue<int> q;
    q.push(10);
    q.push(20);

    int a = q.front();
    q.pop();

    cout << a << endl;

    a = q.front();
    q.pop();
    cout << a << endl;

    stack<int> s;
    s.push(1);
    s.push(2);
    cout << s.top() << endl;
    s.pop();
    s.pop();
    s.pop();
    cout << s.top() << endl;


    return 0;
}

