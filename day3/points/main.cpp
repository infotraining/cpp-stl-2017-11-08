#include <iostream>
#include <unordered_set>
#include <chrono>
#include <random>
#include <vector>
#include <string>
#include <boost/functional/hash.hpp>

class Point
{
    int x;
    int y;
public:
    Point(int x, int y) : x(x), y(y)
    {
    }

    int getX() const { return x;}
    int getY() const { return y;}
    bool operator ==(const Point& p2) const
    {
        return (x == p2.x) and (y == p2.y);
    }
};

namespace std {
    template<>
    struct hash<Point>
    {
        size_t operator()(const Point& p) const
        {
//            size_t seed = 0;
//            boost::hash_combine(seed, p.getX());
//            boost::hash_combine(seed, p.getY());
//            return seed;
            //return p.getX() + p.getY();
            //return ( 37 * p.getX()) + (p.getY() * 31);
            return ( 37 * p.getX()) + (p.getY() * 31);
        }
    };
}

using namespace std;


template<typename Cont>
void print_stats(Cont cnt)
{
    cout << "size: " << cnt.size() << endl;
    cout << "load factor: " << cnt.load_factor() << endl;
    cout << "bucket count: " << cnt.bucket_count() << endl;
}


int main()
{
    mt19937_64 mt(2017);
    uniform_int_distribution<> dist(0, 100);

    vector<pair<int, int>> v;
    vector<Point> v2;

    for (int i = 0 ; i < 10000 ; ++i)
    {
        v.emplace_back(dist(mt), dist(mt));
        v2.emplace_back(dist(mt), dist(mt));
    }

    auto start = chrono::high_resolution_clock::now();
    unordered_set<Point> s;
    for (int i = 0 ; i < 10000 ; ++i)
    {
        s.emplace(v[i].first, v[i].second);
    }
    auto end = chrono::high_resolution_clock::now();
    cout << "creating: " << chrono::duration_cast<chrono::microseconds>(end-start).count() << endl;

    int how_many{};

    start = chrono::high_resolution_clock::now();
    for (int i = 0 ; i < 10000 ; ++i)
    {
        how_many += s.count(v2[i]);
    }
    end = chrono::high_resolution_clock::now();

    cout << "finding: " << chrono::duration_cast<chrono::microseconds>(end-start).count() << endl;

    print_stats(s);

    return 0;
}
