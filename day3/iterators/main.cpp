#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

template<typename Cnt>
void print(Cnt cnt)
{
    for(auto& el : cnt)
        cout << el << ", ";
    cout << endl;
}

int main()
{
    vector<int> v = {1,2,3,4,5,6};
    vector<int> out;

    out.resize(v.size());
    copy(begin(v), end(v), begin(out));
    print(v);
    print(out);

    out.clear();
    //out.resize(v.size());
    back_insert_iterator<vector<int>> bii(out);
    copy_if(begin(v), end(v), bii, [](int x){return x % 2;});
    print(out);

    copy_if(begin(v), end(v), ostream_iterator<int>(cout, "- "),
            [](int x){return not (x % 2);});
    cout << endl << "**************" << endl;

    auto new_end = remove_if(begin(v), end(v), [](int x){return x < 4;});
    v.erase(new_end, end(v));
    print(v);

    return 0;
}
