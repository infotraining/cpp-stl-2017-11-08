#include <iostream>
#include <stack>
#include <vector>
#include <functional>
#include <sstream>
#include <map>

using namespace std;

double test(double a, double b)
{
    return 1.0;
}

struct Test
{
    double operator()(double a, double b)
    {
        return 1.0;
    }
};

template<typename It>
double evaluate(It it, It end)
{
    map<string, function<double(double, double)>> ops;
    stack<double> stck;

    ops["+"] = [](double a, double b) { return a+b;};
    ops["-"] = [](double a, double b) { return a-b;};
    ops["*"] = [](double a, double b) { return a*b;};
    ops["/"] = [](double a, double b) { return a/b;};

    auto pop = [&stck]() {auto a = stck.top(); stck.pop(); return a;};

    for ( ; it != end ; ++it)
    {
        stringstream ss(*it);
        double val{};
        if (ss >> val)
        {
            stck.push(val);
        }
        else
        {
            double r = pop();
            double l = pop();
            stck.push(ops[*it](l, r));
        }
    }
    // double a = 0.0;
    //return stck.top(); // WTF??
}

int main()
{
    vector<string> s1{"2", "2","+"};
    vector<string> s2{"3", "2", "1", "+", "*", "2", "/"}; // = 4.5
    auto a = evaluate(s1.begin(), s1.end());
    cout << a << endl;
    cout << evaluate(s2.begin(), s2.end()) << endl;

    auto l  = [](double x, double y) -> double { return x + y;};
    std::function<double(double, double)> f = l;
    f = test;
    f = Test();
    f(3,4);

    return 0;
}
