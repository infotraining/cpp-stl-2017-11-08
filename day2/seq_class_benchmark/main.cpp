#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <random>
#include <chrono>
#include <algorithm>

using namespace std;

std::mt19937 gen(2017);

long get_rand()
{
    std::uniform_int_distribution<> dis(1, 1000);
    return dis(gen);
}

vector<vector<long>> table;

class ExpensiveObj
{
private:
    vector<long> v_;
public:
    ExpensiveObj(vector<long> v) : v_(v) {}

    ExpensiveObj(ExpensiveObj&& obj) = default;
    ExpensiveObj& operator=(ExpensiveObj&& obj) = default;

    ExpensiveObj(const ExpensiveObj& obj)
    {
        v_ = obj.v_;
    }

    ExpensiveObj& operator=(const ExpensiveObj& obj)
    {
        v_ = obj.v_;
        return *this;
    }


    bool operator<(const ExpensiveObj& obj)
    {
        return v_[0] < obj.v_[0];
    }

    ~ExpensiveObj()
    {}

};

template<typename Cont>
void sort(Cont cont)
{
    std::sort(std::begin(cont), std::end(cont));
}

template<typename T>
void sort(list<T> list)
{
    list.sort();
}

template<typename Cont>
void test(Cont& cnt, size_t size)
{
    auto start = chrono::high_resolution_clock::now();
    for (size_t i = 0 ; i< size ; ++i)
    {
        ExpensiveObj obj{table[i]};
        cnt.push_back(obj);
    }
    auto end = chrono::high_resolution_clock::now();
    cout << "elapsed " << chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;
    start = chrono::high_resolution_clock::now();
    sort(cnt);
    end = chrono::high_resolution_clock::now();
    cout << "elapsed sort: " << chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;
}

int main()
{
    size_t size = 20000;
    for (int i = 0 ; i < size; ++i)
    {
        vector<long> tmp;
        for (int j = 0 ; j < 1000 ; ++j)
        {
            tmp.push_back(get_rand());
        }
        table.push_back(tmp);
    }

    vector<ExpensiveObj> v;
    v.reserve(size);
    cout << "Vector" << endl;
    test(v, size);

    deque<ExpensiveObj> d;
    cout << "Deque" << endl;
    test(d, size);

    list<ExpensiveObj> l;
    cout << "List" << endl;
    test(l, size);
    return 0;
}
