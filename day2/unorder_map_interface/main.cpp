#include <iostream>
#include <unordered_set>
#include <random>

using namespace std;

template<typename Cont>
void print_stats(Cont cnt)
{
    cout << "size: " << cnt.size() << endl;
    cout << "load factor: " << cnt.load_factor() << endl;
    cout << "bucket count: " << cnt.bucket_count() << endl;

    for (int i = 0 ; i < cnt.bucket_count() ; ++i)
    {
        cout << "bucket " << i << " has " << cnt.bucket_size(i) << endl;
    }
}

int main()
{
    random_device rd;
    mt19937_64 mt(rd());
    uniform_int_distribution<> dist(0, 10000);

    unordered_set<int> s;
    for (int i = 0 ; i < 1000 ; i++)
    {
        s.emplace(dist(mt));
    }
    print_stats(s);
    for (int i = 0 ; i < 1000 ; i++)
    {
        s.emplace(dist(mt));
    }
    print_stats(s);
    return 0;
}
