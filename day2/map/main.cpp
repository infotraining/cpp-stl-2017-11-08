#include <iostream>
#include <map>

using namespace std;

std::tuple<bool, int, double> res()
{
    return {true, 1, 3.14};
}

int main()
{
    map<string, double> stock;
    stock.insert(pair<string, double>("NKIA", 0.001));
    stock.insert(make_pair("IBM", 1.00));
    stock.insert(pair("APPL", 2.00));

    stock["INTL"] = 100;
    stock["INTL"] = 10000;

    stock.insert(make_pair("IBM", 10.00));

    cout << stock["Leszek"] << endl;

    for(auto& el : stock)
    {
        cout << el.first << " " << el.second << endl;
    }

//    for(auto& el : stock)
//    {
//        auto [name, value] = el;
//        cout << name << " " << value << endl;
//    }

//    auto [bool_val, number, pi] = res();

    bool a;
    int b;
    double c;
    std::tie(a, b, c) = res();

    return 0;
}
