#include <iostream>
#include <set>
#include <unordered_set>
#include <fstream>
#include <string>
#include <vector>
#include <chrono>

using namespace std;

template<typename Cont>
void print_stats(Cont cnt)
{
    cout << "size: " << cnt.size() << endl;
    cout << "load factor: " << cnt.load_factor() << endl;
    cout << "bucket count: " << cnt.bucket_count() << endl;

//    for (int i = 0 ; i < cnt.bucket_count() ; ++i)
//    {
//        cout << "bucket " << i << " has " << cnt.bucket_size(i) << endl;
//    }
}

int main()
{
    vector<string> sentence{"this","is", "almost", "goood","snetence"};

    ifstream inp("en.dict");
    if(!inp){ cerr << "error opening file" << endl; return -1;}
    string word;
    //set<string> dict;

    auto start = chrono::high_resolution_clock::now();

    unordered_set<string> dict;
    dict.rehash(50000);
    while(inp >> word)
    {
        dict.insert(word);
    }
    auto end = chrono::high_resolution_clock::now();
    cout << "dict size = " << dict.size() << endl;

    cout << "elapsed sort: " << chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;

    for (const auto& word : sentence)
    {
        if (not dict.count(word))
        {
            cout << "misspelled: " << word << endl;
        }
    }
    //dict.rehash(50000);
    print_stats(dict);


    return 0;
}
