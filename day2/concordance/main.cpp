#include <iostream>
#include <map>
#include <unordered_map>
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <vector>
#include <chrono>

using namespace std;

void clean(string& word)
{
    boost::to_lower(word);
    boost::trim_if(word, boost::is_any_of("\"'\t?!,.;:()-/\\"));
}

int main()
{
    unordered_map<string, int> freq_map;
    ifstream inp("proust.txt");
    if(!inp){ cerr << "error opening file" << endl; return -1;}
    string word;
    auto start = chrono::high_resolution_clock::now();

    while (inp >> word)
    {
        clean(word);
        freq_map[word]++;
    }
    auto end = chrono::high_resolution_clock::now();
    cout << "elapsed loading: " << chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;

//    for(auto& el : freq_map)
//    {
//        cout << el.first << " " << el.second << endl;
//    }

    map<int, vector<string_view>, greater<int>> results;
    /*for(auto& [word, frq] : freq_map)
    {
        results[frq].emplace_back(word);
    }*/
    start = chrono::high_resolution_clock::now();
    for (auto& el : freq_map)
    {
        results[el.second].emplace_back(el.first);
    }

    end = chrono::high_resolution_clock::now();
    cout << "elapsed second map: " << chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;


//    for (auto& el : results)
//    {
//        cout << el.first << "\t\t";
//        for (auto& words : el.second) cout << words << " ";
//        cout << endl;
//    }
    return 0;
}
