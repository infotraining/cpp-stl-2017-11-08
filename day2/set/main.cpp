#include <iostream>
#include <set>

using namespace std;

int main()
{
    set<int, greater<int>> s;

    s.insert(1);
    s.insert(5);
    s.insert(22);
    s.insert(10);

    if (s.insert(10).second)
    {
        cout << "inserted" << endl;
    }
    else
    {
        cout << "not inserted" << endl;
    }

    cout << s.count(10) << endl;
    cout << "******" << endl;
    for (auto& el : s)
    {
        cout << el << endl;
    }

    return 0;
}
