#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <random>
#include <chrono>
#include <algorithm>

std::mt19937 gen(2017);

long get_rand()
{
    std::uniform_int_distribution<> dis(1, 1000);
    return dis(gen);
}

long* table;

void prepare_cont(size_t size)
{
    table = new long[size];
    for (size_t i = 0 ; i < size ; ++i)
    {
        table[i] = get_rand();
    }
}

using namespace std;

template<typename Cont>
void sort(Cont cont)
{
    std::sort(std::begin(cont), std::end(cont));
}

template<typename T>
void sort(list<T> list)
{
    list.sort();
}

template<typename Cont>
void test(Cont& cnt, size_t size)
{
    auto start = chrono::high_resolution_clock::now();
    for (size_t i = 0 ; i< size ; ++i)
        cnt.push_back(table[i]);
    auto end = chrono::high_resolution_clock::now();
    cout << "elapsed " << chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;

    start = chrono::high_resolution_clock::now();
    sort(cnt);
    end = chrono::high_resolution_clock::now();
    cout << "elapsed sort: " << chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;
}

int main()
{    
    for (size_t size = 100'000 ; size < 100'000'000 ; size*= 10)
    {
         cout << "Vector " << size << endl;
         prepare_cont(size);
         vector<long> v;
         v.reserve(size);
         test<vector<long>>(v, size);

         cout << "Deque " << size << endl;
         deque<long> d;
         test<deque<long>>(d, size);

         cout << "List " << size << endl;
         list<long> l;
         test<list<long>>(l, size);

         delete[] table;
    }
    return 0;
}
